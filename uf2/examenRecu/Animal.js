/*
[
    ["s",0,"salvatge1","tigre",25,"Zoo de Barcelona"],
    ["s",1,"salvatge2","lleo",30,"Zoo de Girona"],
    ["s",2,"salvatge3","elefant",70,"Zoo de fora"],
    ["d",10,"domestic1","gat",20,123456,"Pepe"],
    ["d",11,"domestic2","gos",10,13435,"Maria"],
    ["d",12,"domestic3","periquito", 5,246534,"Ivan"]
]
*/

class Animal {
    constructor(codi, nom, especie, esperançaDeVida) {
        this.codi = codi;
        this.nom = nom;
        this.especie = especie;
        this.esperançaDeVida = esperançaDeVida;
        this.dataDarreraRevisio = undefined;
        this.dataProperaRevisio = undefined;
        this.veterinariResponsable = "Pujol";
        this.diesEntreRevisio = 90;
    }

    calcularDataProperaRevisio() {
        if (this.dataDarreraRevisio == undefined) {
            let date = new Date();

            date.setDate(date.getDate() + 10);

            this.dataProperaRevisio = date;

            return this.dataProperaRevisio;
        } else {
            let dateDarreraRevisio = new Date(this.dataDarreraRevisio);
            let date = new Date();
            let todaysDate = new Date();

            date.setDate(dateDarreraRevisio.getDate() + this.diesEntreRevisio);

            if (todaysDate < date) {
                date.setDate(todaysDate.getDate() + 3);
                this.dataProperaRevisio = date;
            } else {
                this.dataProperaRevisio = date;
            }

            return this.dataProperaRevisio;
        }
    }

    llistarDadesProperaRevisio() {
        //let dataPropera = calcularDataProperaRevisio();

        let text = this.codi + " " +
            this.nom + " " +
            this.especie + " " +
            this.esperançaDeVida + " " +
            this.dataDarreraRevisio + " " +
            this.dataProperaRevisio + " " +
            this.veterinariResponsable + " " +
            this.diesEntreRevisio;

        return text;
    }
}

class AnimalSalvatge extends Animal {
    constructor(codi, nom, especie, esperançaDeVida, nomZooDeOrigen) {
        super(codi, nom, especie, esperançaDeVida);
        this.nomZooDeOrigen = nomZooDeOrigen;
    }

    llistarDadesProperaRevisio() {
        //let dataPropera = this.calcularDataProperaRevisio();

        let text = this.codi + " " +
            this.nom + " " +
            this.especie + " " +
            this.esperançaDeVida + " " +
            this.dataDarreraRevisio + " " +
            this.dataProperaRevisio + " " +
            this.veterinariResponsable + " " +
            this.diesEntreRevisio + " " +
            this.nomZooDeOrigen;

        return text;
    }
}

class AnimalDomestic extends Animal {
    constructor(codi, nom, especie, esperançaDeVida, xip, cuidadorExtern) {
        super(codi, nom, especie, esperançaDeVida);
        this.diesEntreRevisio = 180;
        this.xip = xip;
        this.cuidadorExtern = cuidadorExtern;
    }

    llistarDadesProperaRevisio() {
        //let dataPropera = this.calcularDataProperaRevisio();

        let text = this.codi + " " +
            this.nom + " " +
            this.especie + " " +
            this.esperançaDeVida + " " +
            this.dataDarreraRevisio + " " +
            this.dataProperaRevisio + " " +
            this.veterinariResponsable + " " +
            this.diesEntreRevisio + " " +
            this.xip + " " +
            this.cuidadorExtern;

        return text;
    }
}

class Zoo {
    /*constructor() {
    }*/

    constructor(nomZoo, adreça, objectesAnimal) {
        this.nomZoo = nomZoo;
        this.adreça = adreça;
        this.objectesAnimal = objectesAnimal;
    }

    registrarAnimals(arrayAnimals) {
        let animals = new Array(arrayAnimals.length);

        for (let i = 0; i < arrayAnimals.length; i++) {
            if (arrayAnimals[i][0] == "s") {
                let newAnimal = new AnimalSalvatge(arrayAnimals[i][1], arrayAnimals[i][2], arrayAnimals[i][3], arrayAnimals[i][4], arrayAnimals[i][5]);
                animals.push(newAnimal);
            } else if (arrayAnimals[i][0] == "d") {
                let newAnimal = new AnimalDomestic(arrayAnimals[i][1], arrayAnimals[i][2], arrayAnimals[i][3], arrayAnimals[i][4], arrayAnimals[i][5], arrayAnimals[i][6]);
                animals.push(newAnimal);
            }
        }

        this.objectesAnimal = animals;
    }

    generarLlistat() {
        this.objectesAnimal.forEach(element => {
            let date = new Date();
            //console.log(element);

            element.dataProperaRevisio = element.calcularDataProperaRevisio();

            if (element.dataProperaRevisio > date) {
                let div = document.getElementById("div");
                let p = document.createElement("p");

                p.innerHTML = element.llistarDadesProperaRevisio();

                div.appendChild(p);
            }
        });
    }
}