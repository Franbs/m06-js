class Car {
    constructor(speed) {
        this.speed = speed;
    }

    accelerate() {
        this.speed += 10;
        return this.speed;
    }

    brake() {
        this.speed -= 5;
        return this.speed;
    }
}