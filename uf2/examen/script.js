function parseJSON(text) {
    let jsonArray = JSON.parse(text);
    let zoo = new Zoo('ZooXYZ', 'Calle xyz', jsonArray);

    zoo.registrarAnimals(jsonArray);

    zoo.generarLlistat();
}