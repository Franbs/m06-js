class Animal {
    constructor(codi, nom, especie, fechaNaixement, esperanzaDeVida) {
        this.codi = codi;
        this.nom = nom;
        this.especie = especie;
        this.fechaNaixement = fechaNaixement;
        this.esperanzaDeVida = esperanzaDeVida;
        this.fechaUltimaRevision = "undefined";
        this.veterinarioResponsable = "Pujol";
        this.diasEntreRevisiones = 90;
    }
    
    dataProperaRevisio() {
        let string = this.codi + " " +
            this.nom + " " +
            this.especie + " " +
            this.fechaNaixement + " " +
            this.esperanzaDeVida + " " +
            this.fechaUltimaRevision + " " +
            this.veterinarioResponsable + " " +
            this.diasEntreRevisiones;

        console.log(string);

        return string;
    }
}

class AnimalDomestic extends Animal {
    constructor(codi, nom, especie, fechaNaixement, esperanzaDeVida, chip, cuidadorExterno) {
        super(codi, nom, especie, fechaNaixement, esperanzaDeVida);
        this.chip = chip;
        this.cuidadorExterno = cuidadorExterno;
        this.fechaUltimaRevision = "undefined";
        this.veterinarioResponsable = "Pujol";
        this.diasEntreRevisiones = 180;
    }

    dataProperaRevisio() {
        let string = this.codi + " " +
            this.nom + " " +
            this.especie + " " +
            this.fechaNaixement + " " +
            this.esperanzaDeVida + " " +
            this.fechaUltimaRevision + " " +
            this.veterinarioResponsable + " " +
            this.diasEntreRevisiones;

        string += " " + this.chip + " " + this.cuidadorExterno;
        console.log(string);

        return string;
    }
}

class AnimalSalvatge extends Animal {
    constructor(codi, nom, especie, fechaNaixement, esperanzaDeVida, zooDeReferencia) {
        super(codi, nom, especie, fechaNaixement, esperanzaDeVida);
        this.zooDeReferencia = zooDeReferencia;
    }

    dataProperaRevisio() {
        let string = this.codi + " " +
            this.nom + " " +
            this.especie + " " +
            this.fechaNaixement + " " +
            this.esperanzaDeVida + " " +
            this.fechaUltimaRevision + " " +
            this.veterinarioResponsable + " " +
            this.diasEntreRevisiones;

        string += " " + this.zooDeReferencia;
        console.log(string);

        return string;
    }
}

class Zoo {
    constructor(nomZoo, adreça, animalsArray) {
        this.nomZoo = nomZoo;
        this.adreça = adreça;
        this.animalsArray = animalsArray;
    }

    registrarAnimals(jsonArray) {
        this.animalsArray = new Array(jsonArray.length);

        for (let i = 0; i < jsonArray.length; i++) {
            if (Object.keys(jsonArray[i]).length == 7) {
                let tmpAnimal = new AnimalDomestic(jsonArray[i].codi, jsonArray[i].nom, jsonArray[i].especie, jsonArray[i].fechaNaixement, jsonArray[i].esperanzaDeVida, jsonArray[i].chip, jsonArray[i].cuidadorExterno);
                animalsArray.push(tmpAnimal);
                delete jsonArray[i];

            } else if (Object.keys(jsonArray[i]).length == 5) {
                let tmpAnimal = new Animal(jsonArray[i].codi, jsonArray[i].nom, jsonArray[i].especie, jsonArray[i].fechaNaixement, jsonArray[i].esperanzaDeVida);
                animalsArray.push(tmpAnimal);
                delete jsonArray[i];

            } else {
                let tmpAnimal = new AnimalSalvatge(jsonArray[i].codi, jsonArray[i].nom, jsonArray[i].especie, jsonArray[i].fechaNaixement, jsonArray[i].esperanzaDeVida, jsonArray[i].zooDeReferencia);
                animalsArray.push(tmpAnimal);
                delete jsonArray[i];
            }
        }
    }

    generarLlistat() {
        this.animalsArray.forEach(element => {
            element.dataProperaRevisio();
        });
    }
}