// [[[1,2,3], [4,5,6], [7,8,9]], [[9,8,7], [6,5,4], [3,2,1]]]

// No funcionan
// [[0,0,0,7,0,6,3,9,0], [7,0,0,4,0,0,0,8,2], [0,5,0,0,0,3,0,0,0], [0,4,5,0,0,0,0,6,3], [0,0,0,8,0,0,2,0,0], [3,0,2,0,0,4,0,0,0], [0,6,0,0,9,8,0,0,0], [5,8,7,0,2,1,4,3,0], [9,0,3,0,4,7,6,1,8]]

// Funcionan
// [[0,0,3,2,0,0,0,1,0], [0,1,0,0,6,9,0,7,5], [0,9,6,0,7,5,3,0,2], [0,6,0,0,5,0,0,0,8], [0,8,0,0,3,6,2,4,9], [0,7,0,9,0,0,0,0,0], [0,0,1,0,0,7,0,9,0], [0,0,0,0,0,0,5,2,7], [8,2,7,0,9,0,1,6,3]]
// [[0,1,7,0,4,5,0,0,0], [0,9,4,0,2,0,8,5,1], [0,6,0,8,0,0,0,7,3], [1,2,0,0,0,0,6,8,0], [5,0,9,1,6,2,0,0,4], [0,4,0,0,0,8,0,1,9], [0,0,2,9,1,3,0,0,0], [0,0,1,6,8,0,0,0,2], [0,0,0,2,0,0,0,9,0]]
// [[0,0,0,0,0,0,1,0,0], [0,8,1,0,9,3,0,4,5], [4,0,0,0,5,0,0,3,2], [9,0,0,5,0,4,0,7,8], [0,5,8,3,0,0,0,0,9], [0,0,0,8,2,0,0,0,0], [5,0,0,2,7,0,0,8,4], [6,0,4,1,3,8,2,0,7], [8,0,2,9,0,0,6,0,0]]
// [[1,3,2,0,8,0,0,7,4], [0,0,8,7,3,0,2,0,0], [7,6,4,0,0,0,8,0,0], [4,2,0,5,0,0,0,1,9], [0,0,5,3,0,0,7,0,0], [6,0,0,1,0,0,0,5,2], [0,0,6,4,7,3,0,0,0], [0,4,9,0,0,1,0,2,0], [8,0,1,0,2,0,0,0,3]]
// [[2,0,9,0,6,0,0,3,8], [0,0,4,5,0,8,1,0,0], [6,0,5,0,0,0,4,0,9], [0,5,0,3,4,0,0,2,0], [0,0,0,0,0,1,0,0,0], [4,7,0,0,0,6,8,9,1], [0,0,0,4,2,0,9,1,3], [0,4,2,9,1,0,6,0,0], [0,9,7,0,0,0,2,0,4]]

function sleep(time) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > time) {
            break;
        }
    }
}

function createBoard(string) {
    if (string != "") {
        for (let i = 0; i < 9; i++) {
            let tmpIdTr = "tr" + (i + 1);
            let td = document.getElementById(tmpIdTr).childNodes;
    
            for (let j = 0; j < 9; j++) {
                td[j].style.color = "black";
            }
        }
        
        var sudoku = new Array(9);

        for (x = 0; x < sudoku.length; x++) {
            sudoku[x] = new Array(9);
        }

        string = string.slice(1, -1).split(", ");

        for (let i = 0; i < string.length; i++) {
            sudoku[i] = string[i].slice(1, -1);
            sudoku[i] = sudoku[i].split(",");
        }

        for (let i = 0; i < sudoku.length; i++) {
            for (let x = 0; x < sudoku[i].length; x++) {
                sudoku[i][x] = parseInt(sudoku[i][x]);
            }
        }

        for (let i = 0; i < 9; i++) {
            let tmpIdTr = "tr" + (i + 1);
    
            for (let x = 0; x < 9; x++) {
                let td = document.getElementById(tmpIdTr).childNodes;
                
                if (sudoku[i][x] != 0) {
                    td[x].textContent = sudoku[i][x];
                    td[x].style.visibility = "visible";
                } else {
                    td[x].textContent = sudoku[i][x];
                    td[x].style.visibility = "hidden";
                }
            }
        }
    } else {
        let table = document.createElement("table");
        table.id = "table";
        document.getElementById("body").appendChild(table);

        for (let i = 1; i < 10; i++) {
            let tr = document.createElement("tr");
            tr.id = "tr" + i;
            document.getElementById("table").appendChild(tr);

            for (let x = 1; x < 10; x++) {
                let td = document.createElement("td");
                //let td = document.createElement("tr");
                td.id = "td" + x;
                td.textContent = td.id;
                td.style.visibility = "hidden";
                document.getElementById(tr.id).appendChild(td);
                //document.getElementById(tr.id).appendChild(tdClose);
            }
        }
    }
    
    for (let i = 1; i < 10; i++) {
        let tmpIdTr = "tr" + i;
        let tr = document.getElementById(tmpIdTr);

        if (tr.id == "tr3" || tr.id == "tr6") {
            tr.style.borderBottom = "3px solid black";
        }

        for (let x = 0; x < 9; x++) {
            let td = document.getElementById(tmpIdTr).childNodes;

            if (td[x].id == "td3" || td[x].id == "td6") {
                td[x].style.borderRight = "3px solid black";
                td[x].style.borderBottom = "1px solid black";
            } else {
                td[x].style.border = "1px solid black";
            }

            td[x].style.padding = "5px 5px 5px 5px";
        }
    }
}

function isSudokuSolved(sudoku) {
    for (let i = 0; i < sudoku.length; i++) {
        for (let x = 0; x < sudoku[i].length; x++) {
            if (sudoku[i][x] == 0) {
                return false;
            }
        }
    }

    return true;
}

function checkRow(sudoku, row, posibSol) {
    for (let i = 0; i < 9; i++) {
        if (sudoku[row][i] != 0) {
            restPosibSol(sudoku[row][i], posibSol);
        }
    }
}

function checkColumn(sudoku, column, posibSol) {
    for (let i = 0; i < 9; i++) {
        if (sudoku[i][column] != 0) {
            restPosibSol(sudoku[i][column], posibSol);
        }
    }
}

function checkBox(sudoku, row, column, posibSol) {
    let boxRow = row - row % 3;
    let boxColumn = column - column % 3;

    for (let i = boxRow; i < boxRow + 3; i++) {
        for (let j = boxColumn; j < boxColumn + 3; j++) {
            if (sudoku[i][j] != 0) {
                restPosibSol(sudoku[i][j], posibSol);
            }
        }
    }
}

function fillOneSquare() {
    var sudoku = new Array(9);

    for (x = 0; x < sudoku.length; x++) {
        sudoku[x] = new Array(9);
    }

    for (let i = 0; i < 9; i++) {
        let tmpIdTr = "tr" + (i + 1);
        let td = document.getElementById(tmpIdTr).childNodes;

        for (let j = 0; j < 9; j++) {
            sudoku[i][j] = parseInt(td[j].textContent);
        }        
    }

    let solvedOne = false;

    for (let i = 0; i < 9; i++) {
        for (let j = 0; j < 9; j++) {
            if (sudoku[i][j] == 0) {
                let posibSol = [1, 2, 3, 4, 5, 6, 7, 8, 9];

                checkRow(sudoku, i, posibSol);
                checkColumn(sudoku, j, posibSol);
                checkBox(sudoku, i, j, posibSol);

                if (posibSol.length == 1) {
                    let tmpIdTr = "tr" + (i + 1);

                    let td = document.getElementById(tmpIdTr).childNodes;

                    td[j].textContent = posibSol[0] + " ";
                    td[j].style.color = "blue";
                    td[j].style.visibility = "visible";

                    sudoku[i][j] = posibSol[0];

                    solvedOne = true;
                    break;
                }

                //sleep(100);
            }
        }

        if (solvedOne) {
            break;
        }
    }
}

function solveSudoku(string) {
    var sudoku = new Array(9);

    for (x = 0; x < sudoku.length; x++) {
        sudoku[x] = new Array(9);
    }

    for (let i = 0; i < 9; i++) {
        let tmpIdTr = "tr" + (i + 1);
        let td = document.getElementById(tmpIdTr).childNodes;

        for (let j = 0; j < 9; j++) {
            sudoku[i][j] = parseInt(td[j].textContent);
        }        
    }

    while (!isSudokuSolved(sudoku)) {
        for (let i = 0; i < 9; i++) {
            for (let j = 0; j < 9; j++) {
                if (sudoku[i][j] == 0) {
                    let posibSol = [1, 2, 3, 4, 5, 6, 7, 8, 9];

                    console.log(posibSol);
                    checkRow(sudoku, i, posibSol);
                    console.log(posibSol);
                    checkColumn(sudoku, j, posibSol);
                    console.log(posibSol);
                    checkBox(sudoku, i, j, posibSol);
                    console.log(posibSol);

                    if (posibSol.length == 1) {
                        let tmpIdTr = "tr" + (i + 1);

                        let td = document.getElementById(tmpIdTr).childNodes;

                        td[j].textContent = posibSol[0] + " ";
                        td[j].style.color = "blue";
                        td[j].style.visibility = "visible";

                        sudoku[i][j] = posibSol[0];
                    }

                    //sleep(100);
                }
            }
        }
    }
}

function restPosibSol(num, posibSol) {
    for (let x = 0; x < posibSol.length; x++) {
        if (posibSol[x] == num) {
            posibSol.splice(x, 1);
        }
    }
}

function multiplicar(string) {
    var sudokuString = JSON.parse(string);

    var m1 = sudokuString[0];
    var m2 = sudokuString[1];

    console.table(m1);
    console.table(m2);

    var mT = new Array(m1.length);

    for (x = 0; x < mT.length; x++) {
        mT[x] = new Array(m1[x].length);
    }

    for (let row = 0; row < m1.length; row++) {
        for (let column = 0; column < m1[row].length; column++) {
            mT[row][column] = 0;
            for (let z = 0; z < m2[0].length; z++) {
                mT[row][column] += /*mT[row][column] + */m1[row][z] * m2[z][column];
            }
        }
    }

    console.table(mT);
}