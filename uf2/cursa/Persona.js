class Persona {
    constructor(nom, cognom, dataNaixement, dni, telefon, sexe) {
        this.nom = nom;
        this.cognom = cognom;
        this.dataNaixement = dataNaixement;
        this.dni = dni;
        this.telefon = telefon;
        this.sexe = sexe;
    }
}

class Corredor extends Persona {
    constructor(nom, cognom, dataNaixement, dni, telefon, sexe, dorsal, federat, rendiment) {
        super(nom, cognom, dataNaixement, dni, telefon, sexe);
        this.dorsal = dorsal;
        this.federat = federat;
        this.rendiment = rendiment;
    }

    iniciCursa() {

    }

    avançarDistancia() {

    }

    passarControl() {

    }
}

class Jutge extends Persona {
    constructor(nom, cognom, dataNaixement, dni, telefon, sexe) {
        super(nom, cognom, dataNaixement, dni, telefon, sexe);
    }

    sancionarCorredor() {

    }

    registrarRetirada() {

    }
}

class Voluntari extends Persona {
    constructor(nom, cognom, dataNaixement, dni, telefon, sexe) {
        super(nom, cognom, dataNaixement, dni, telefon, sexe);
    }
}

class Cursa {
    constructor(horaInici) {
        this.horaInici = horaInici;
    }

    inscriureCorredors() {

    }

    iniciarCursa() {

    }
}

class Categoria extends Cursa {
    constructor(horaInici, nom, sexe, edat_min, edat_max, inscrits) {
        super(horaInici);
        this.nom = nom;
        this.sexe = sexe;
        this.edat_min = edat_min;
        this.edat_max = edat_max;
        this.inscrits = inscrits;
    }

    generarClassificacio() {
        
    }
}