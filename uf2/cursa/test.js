function iniciarCarrera(json) {
    clear();

    json += "]";
    console.log(json);

    let jsonArray = JSON.parse(json);

    console.log(jsonArray);

    for (let i = 0; i < jsonArray.length; i++) {
        let date = new Date();
        
        let birthdate = date.getFullYear() - jsonArray[i].age;

        let dniStr = jsonArray[i].dni;
        checkDni(dniStr);

        let phoneNumber = jsonArray[i].phone;
        checkPhone(phoneNumber);

        jsonArray[i] = new Corredor(jsonArray[i].name, jsonArray[i].surname, birthdate, jsonArray[i].dni, jsonArray[i].phone, jsonArray[i].gender, jsonArray[i].dorsal, jsonArray[i].federat, jsonArray[i].rendiment);
    }

    openWindow(jsonArray);
}

function checkDni(dniStr) {
    if (dniStr.length == 9) {
        var numDni = parseInt(dniStr.slice(0,8));
        var letraDni = dniStr.charAt(8);

        var numLetra = numDni % 23;

        let arrayLetras = ["T", "R", "W", "A", "G", "M", "Y", "F", "P", "D", "X", "B", "N", "J", "Z", "S", "Q", "V", "H", "L", "C", "K", "E"];

        if (letraDni.toUpperCase() == arrayLetras[numLetra].toUpperCase()) {
            
        } else {
            alert("El dni no es correcte");
            location.reload();
        }
    } else {
        alert("El valor introduit no es valid");
        location.reload();
    }
}

function checkPhone(phoneNumber) {
    if (isNaN(phoneNumber) || phoneNumber.toString().length != 9) {
        alert("El valor introduit no es valid");
        location.reload();
    }
}

function clear() {
    document.getElementById('name').value = "";
    document.getElementById('surname').value = "";
    document.getElementById('age').value = "";
    document.getElementById('dni').value = "";
    document.getElementById('phone').value = "";
    document.getElementById('gender').value = "";
    document.getElementById('dorsal').value = "";
    document.getElementById('federat').value = "";
    document.getElementById('rendiment').value = "";
}

function openWindow(jsonArray) {
    let top = 0;
    let left = 0;
    let windowArray = new Array();
    console.log(jsonArray.length);
    console.log(windowArray.length);
    
    jsonArray.forEach(element => {
        let currentWindow = window.open("", "","top=" + top + ",left=" + left + ",width=400,height=400,background-color=gray");
        currentWindow.document.writeln("<body bgcolor='#587C68'>");
        windowArray.push(currentWindow);
        left += 300;
        top += 50;
    });

    console.log(windowArray);

    startRunning(windowArray);
}

function addCorredor(name, surname, age, dni, phone, gender, dorsal, federat, rendiment) {
    if (document.getElementById('textArea').textContent != "") {
        let content = ',{"name": "' + name + '","surname": "' + surname + '","age": "' + age + '","dni": "' + dni + '","phone": "' + phone + '","gender": "' + gender + '","dorsal": ' + dorsal + ',"federat": "' + federat + '","rendiment": "' + rendiment + '"}';

        let finalString = content;
        document.getElementById('textArea').textContent += finalString;
    } else {
        let content = '[{"name": "' + name + '","surname": "' + surname + '","age": "' + age + '","dni": "' + dni + '","phone": "' + phone + '","gender": "' + gender + '","dorsal": ' + dorsal + ',"federat": "' + federat + '","rendiment": "' + rendiment + '"}';

        let finalString = content;
        document.getElementById('textArea').textContent += finalString;
    }
}

function startRunning(windowArray) {
    windowArray.forEach(element => {
        element.document.writeln("<body bgcolor='#000000'>");
    });

    /*for (let i = 0; i < windowArray.length; i++) {
        windowArray[i].document.writeln("<body bgcolor='#000000'>");
    }*/
}