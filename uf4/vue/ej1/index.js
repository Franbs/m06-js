new Vue({
    el: '#app',
    data: {
        text: "",
        terms: false
    },
    methods: {
        disableButton() {
            return (this.text.length < 10 || this.text.length >= 100);
        },

        changeColor() {
            var str = "background-color: ";
            var length = this.text.length;

            if (length < 10 || length >= 100) {
                return str + "red";
            } else if (length >= 50 && length < 70) {
                return str + "yellow";
            } else if (length >= 70) {
                return str + "orange";
            } else {
                return str + "green";
            }
        },

        remainingChars() {
            return (100 - this.text.length);
        }
    }
})