var app = new Vue({ 
    el: '#app',
    data: {
        message: "",
        currentImage: 1,
        text: "",
        velocidad: 0,
        intermitenteL: "off",
        intermitenteR: "off",
        hayAveria: "no",
        interval: ""
    },
    methods: {
        changeImageFunction() {
            let str = "./llave";
            let strEnd = ".png"
            let strFinal = "";
            
            if (this.currentImage > 1) {
                strFinal = str + this.currentImage + strEnd;
                document.getElementById("keyImage").src = strFinal;
                this.text = strFinal;
                this.currentImage = 1;
            } else {
                strFinal = str + this.currentImage + strEnd;
                document.getElementById("keyImage").src = strFinal;
                this.currentImage++;
                this.text = strFinal;
            }
        }, 

        disableElement() {
            return (this.text !== "./llave1.png");
        },

        startTimer() {
            this.velocidad += 10;
            console.log(this.velocidad);
        },

        accelera() {
            clearInterval(this.interval);
            this.interval = setInterval(this.startTimer(), 1000);
        },

        frena() {
            this.velocidad -= 10;
            console.log(this.velocidad);
        },

        clear() {
            //clearInterval(this.interval);
        },

        cambiarIntermitenteL() {
            let str = "./llum";
            let strEnd = ".jpeg"
            let strFinal = "";
            
            if (this.intermitenteL === "off") {
                strFinal = str + "on" + strEnd;
                document.getElementById("llumImageL").src = strFinal;
                this.intermitenteL = "on";
            } else {
                strFinal = str + "off" + strEnd;
                document.getElementById("llumImageL").src = strFinal;
                this.intermitenteL = "off";
            }
        }, 

        cambiarIntermitenteR() {
            let str = "./llum";
            let strEnd = ".jpeg"
            let strFinal = "";
            
            if (this.intermitenteR === "off") {
                strFinal = str + "on" + strEnd;
                document.getElementById("llumImageR").src = strFinal;
                this.intermitenteR = "on";
            } else {
                strFinal = str + "off" + strEnd;
                document.getElementById("llumImageR").src = strFinal;
                this.intermitenteR = "off";
            }
        }, 

        averia() {
            let str = "./llum";
            let strEnd = ".jpeg"
            let strFinal = "";
            if (this.hayAveria === "no") {
                strFinal = str + "on" + strEnd;
                document.getElementById("llumImageR").src = strFinal;
                document.getElementById("llumImageL").src = strFinal;
                this.hayAveria = "si";
            } else {
                strFinal = str + "off" + strEnd;
                document.getElementById("llumImageR").src = strFinal;
                document.getElementById("llumImageL").src = strFinal;
                this.hayAveria = "no";
            }
        }
    }
});