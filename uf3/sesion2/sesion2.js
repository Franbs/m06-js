const form = document.getElementById('form');

addPassport(document.getElementById('documents'));

addOptionsEstudi();

function addOptionsEstudi() {
    var array = ["Tria un nivell d'estudis","ESO","PFI","Batchillerat","FP"];

    var select = document.getElementById("estudis");

    for (var i = 0; i < array.length; i++) {
        var option = document.createElement("option");
        option.value = array[i];
        option.text = array[i];
        select.appendChild(option);
    }
}

addOptionsTreball();

function addOptionsTreball() {
    var array = ["Tria una situació laboral","Estudiant","Aturat","Pensionista","Treball compte propi", "Treball compte alie"];

    var select = document.getElementById("ocupacio");

    for (var i = 0; i < array.length; i++) {
        var option = document.createElement("option");
        option.value = array[i];
        option.text = array[i];
        select.appendChild(option);
    }
}

form.addEventListener('submit', e => {
	e.preventDefault();

	checkInputs();
});

function addPassport(docs) {
    var option = document.createElement("option");
    option.value = "passport";
    option.text = "Passport";
    docs.appendChild(option);
}

function changeDoc() {
    var docs = document.getElementById('documents');
    
    if (docs.value == 'dni') {
        var doc = document.getElementById('doc');
        formatDoc(doc);
        doc.placeholder = 'DNI';
    } else if (docs.value == 'nie') {
        var doc = document.getElementById('doc');
        formatDoc(doc);
        doc.placeholder = 'NIE';
    } else if (docs.value == 'passport') {
        var doc = document.getElementById('doc');
        formatDoc(doc);
        doc.placeholder = 'Passport';
    } else {
        var doc = document.getElementById('doc');
        doc.value = "";
        doc.type = 'hidden';
        findLabelForControl(doc).style.visibility = 'hidden';
    }
}

function formatDoc(doc) {
    doc.value = "";
    doc.type = 'text';
    findLabelForControl(doc).style.visibility = 'visible';
    doc.setAttribute("maxlength", 8);
    doc.className = "form-control";
}

function findLabelForControl(element) {
    var idVal = element.id;
    labels = document.getElementsByTagName('label');

    for( var i = 0; i < labels.length; i++ ) {
       if (labels[i].htmlFor == idVal)
            return labels[i];
    }
}

function showForm() {
    if (!document.getElementById("divRow")) {
        var divRow = document.createElement('div');
        divRow.setAttribute("id", "divRow");
        divRow.className = "row mt-3 mb-4";
    } else {
        var divRow = document.getElementById("divRow");
    }

    var divCol1 = document.createElement('div');
    divCol1.className = "col me-3";

    var divCol2 = document.createElement('div');
    divCol2.className = "col";

    var divCol3 = document.createElement('div');
    divCol3.className = "col";

    var label1 = document.createElement('label');
    label1.innerHTML = "Tipus de via";

    var input1 = document.createElement('select');
    input1.className = "form-select";

    var array = ["Tria un tipus de via","Carrer","Avinguda","Passeig","Plaça"];

    var label2 = document.createElement('label');
    label2.innerHTML = "Adreça";

    var input2 = document.createElement('input');
    input2.className = "form-control w-100";
    input2.placeholder = "Via Augusta";

    var label3 = document.createElement('label');
    label3.innerHTML = "Numero";

    var input3 = document.createElement('input');
    input3.className = "form-control w-100";
    input3.placeholder = "12";

    divRow.appendChild(divCol1);
    divRow.appendChild(divCol2);
    divRow.appendChild(divCol3);

    divCol1.appendChild(label1);
    divCol1.appendChild(input1);
    for (var i = 0; i < array.length; i++) {
        var option = document.createElement("option");
        option.value = array[i];
        option.text = array[i];
        input1.appendChild(option);
    }

    divCol2.appendChild(label2);
    divCol2.appendChild(input2);

    divCol3.appendChild(label3);
    divCol3.appendChild(input3);

    //document.getElementById("container").appendChild(divRow);
    insertAfter(divRow, document.getElementById("divAddress"));
}

function insertAfter(newNode, referenceNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}

function hideForm() {
    var div = document.getElementById("divRow");
    div.innerHTML = '';
}

function geoBcn() {
    
}

function checkDni(dniStr) {
    var valid = true;

    if (dniStr.length == 9) {
        var numDni = parseInt(dniStr.slice(0,8));
        var letraDni = dniStr.charAt(8);

        var numLetra = numDni % 23;

        let arrayLetras = ["T", "R", "W", "A", "G", "M", "Y", "F", "P", "D", "X", "B", "N", "J", "Z", "S", "Q", "V", "H", "L", "C", "K", "E"];

        if (letraDni.toUpperCase() == arrayLetras[numLetra].toUpperCase()) {
            
        } else {
            /*alert("El dni no es correcte");
            location.reload();*/
            valid = false;
        }
    } else {
        /*alert("El valor introduit no es valid");
        location.reload();*/
        valid = false;
    }

    return valid;
}

function checkInputs() {
    checkClaus();
}

function checkClaus() {
    var clau1 = document.getElementById("clauAcces");
    var clau2 = document.getElementById("clauAccesConfirmation");

    if (clau1.value != clau2.value) {
		setErrorFor(clau1, 'Les claus no són iguals');
	} else {
		setSuccessFor(clau1);
	}
}

function setErrorFor(input, message) {
	const formControl = input.parentElement;
	const small = formControl.querySelector('small');
	formControl.className = 'row error';
	small.innerText = message;
}

function setSuccessFor(input) {
	const formControl = input.parentElement;
	formControl.className = 'row success';
}